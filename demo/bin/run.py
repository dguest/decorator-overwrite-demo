#!/usr/bin/env python3

from AthenaConfiguration.MainServicesConfig import MainServicesCfg as getConfig
from AthenaPoolCnvSvc.PoolReadConfig import PoolReadCfg
from AthenaConfiguration.ComponentFactory import CompFactory
from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator

from GaudiKernel.Configurable import DEBUG

def run():
    from AthenaConfiguration.AllConfigFlags import initConfigFlags
    import AthenaConfiguration
    del AthenaConfiguration.AllConfigFlags.ConfigFlags
    flags = initConfigFlags()

    #################################
    # things you might want to change
    #################################
    #
    # you might want to fiddle with this to get the right input file
    flags.Input.Files = ['test.pool.root']
    # this setting does some interesting things
    #
    # 0 threads: runs
    # 1 thread: runs (different behavior)
    # 2 threads: crash
    flags.Concurrency.NumThreads = 1

    flags.Exec.MaxEvents = 10
    flags.lock()
    ca = getConfig(flags)
    ca.merge(PoolReadCfg(flags))
    ca.addService(CompFactory.AthenaEventLoopMgr(
        EventPrintoutInterval=10))
    ca.addService(CompFactory.ExceptionSvc(Catch="NONE"))

    ca.merge(algs(flags))
    ca.run()

def algs(flags):
    ca = ComponentAccumulator()
    ca.addEventAlgo(
        CompFactory.DecoratorAlg(
            'decorator',
            OutputLevel=DEBUG,
            jetDecorator='AntiKt4EMPFlowJets.thing',
            value=1.0
        )
    )
    ca.addEventAlgo(
        CompFactory.PrinterAlg(
            'printer1',
            OutputLevel=DEBUG,
            jetDecorator='AntiKt4EMPFlowJets.thing'
        )
    )
    ca.addEventAlgo(
        CompFactory.DecoratorAlg(
            'decorator2',
            OutputLevel=DEBUG,
            jetDecorator='AntiKt4EMPFlowJets.thing',
            value=1.4
        )
    )
    ca.addEventAlgo(
        CompFactory.PrinterAlg(
            'printer2',
            OutputLevel=DEBUG,
            jetDecorator='AntiKt4EMPFlowJets.thing'
        )
    )
    return ca

if __name__ == '__main__':
    run()
