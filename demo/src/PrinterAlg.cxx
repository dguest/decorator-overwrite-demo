#include "PrinterAlg.h"
#include "StoreGate/ReadDecorHandle.h"


PrinterAlg::PrinterAlg(
  const std::string& name, ISvcLocator* svcloc):
  AthReentrantAlgorithm(name, svcloc)
{
}


StatusCode PrinterAlg::initialize() {
  ATH_MSG_DEBUG("initialize " << name() << ", jet key " << m_jetKey);
  ATH_CHECK(m_jetKey.initialize());
  return StatusCode::SUCCESS;
}


StatusCode PrinterAlg::execute(const EventContext& cxt) const {

  SG::ReadDecorHandle<xAOD::JetContainer,float> jethandle(m_jetKey, cxt);

  for (const xAOD::Jet* jet: *jethandle) {
    ATH_MSG_INFO("jet decoration: " << jethandle(*jet));
  }

  return StatusCode::SUCCESS;

}

StatusCode PrinterAlg::finalize() {
  return StatusCode::SUCCESS;
}
