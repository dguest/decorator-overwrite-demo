#include "AthenaBaseComps/AthReentrantAlgorithm.h"

#include "xAODJet/JetContainer.h"
#include "StoreGate/ReadDecorHandleKey.h"


class PrinterAlg : public AthReentrantAlgorithm
{
public:
  PrinterAlg(const std::string& name, ISvcLocator* svcloc);
  virtual StatusCode initialize() override;
  virtual StatusCode execute(const EventContext& cxt) const override;
  virtual StatusCode finalize() override;
private:
  SG::ReadDecorHandleKey<xAOD::JetContainer> m_jetKey {
    this, "jetDecorator", "", "name of the decorator"};
};

