#include "AthenaBaseComps/AthReentrantAlgorithm.h"

#include "xAODJet/JetContainer.h"
#include "StoreGate/WriteDecorHandleKey.h"


class DecoratorAlg : public AthReentrantAlgorithm
{
public:
  DecoratorAlg(const std::string& name, ISvcLocator* svcloc);
  virtual StatusCode initialize() override;
  virtual StatusCode execute(const EventContext& cxt) const override;
  virtual StatusCode finalize() override;
private:

  SG::WriteDecorHandleKey<xAOD::JetContainer> m_jetKey {
    this, "jetDecorator", "", "name of the decorator"};
  Gaudi::Property<float> m_value {
    this, "value", 0.0, "value to decroate to jets"
  };
};

