#include "DecoratorAlg.h"
#include "StoreGate/WriteDecorHandle.h"


DecoratorAlg::DecoratorAlg(
  const std::string& name, ISvcLocator* svcloc):
  AthReentrantAlgorithm(name, svcloc)
{
}


StatusCode DecoratorAlg::initialize() {
  ATH_MSG_DEBUG("initialize " << name() << ", jet key " << m_jetKey);
  ATH_CHECK(m_jetKey.initialize());
  return StatusCode::SUCCESS;
}


StatusCode DecoratorAlg::execute(const EventContext& cxt) const {

  SG::WriteDecorHandle<xAOD::JetContainer,float> jethandle(m_jetKey, cxt);

  ATH_MSG_DEBUG(name() << " decorating with " << float(m_value));
  for (const xAOD::Jet* jet: *jethandle) {
    jethandle(*jet) = m_value;
  }

  return StatusCode::SUCCESS;

}

StatusCode DecoratorAlg::finalize() {
  return StatusCode::SUCCESS;
}
