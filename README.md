Decorators are Weird Demo
=========================

This is my attempt to demonstrate the issue I've seen in [ATEAM-909][jira].

It's a minimal project (should compile in `AthAnalysis`) that reads in
a derivation and applies some dummy decorations (any `DAOD_PHYS` file
should work, it just wants pflow jets).

The algorithm sequence here is:

- Read in jets
- Apply a decoration
- Print the decoration
- Overwrite the same decoration (should not be allowed?)
- Print the decoration again

Running
-------

- Find a recent derivation with `AntiKt4EMPFlowJets`
- Rename this `test.pool.root`
- Build this project against `AthAnalysis` (`Athena` should also work)
- run `run.py`

Why I'm confused
----------------

Under `demo/bin/run.py` there's a `ComponentAccumulator`-based script
that runs two algorithms which decorate the same value to jets. The
jets themselves should be locked, and the decoration should lock after
each algorithm exits. But for some reason this doesn't happen: the
second algorithm simply overwrites the first.

Fun with threads
----------------

Things get even weirder when `flags.Concurrency.NumThreads` is set:

- 0 threads: The first print algorithm prints the first value, the
  second prints the second value.
- 1 thread: The algorithms that print always report the second
  value. Both decoration algorithms run.
- 2+ threads: crash (this is the expected behaviour in all cases)


[jira]: https://its.cern.ch/jira/browse/ATEAM-909
